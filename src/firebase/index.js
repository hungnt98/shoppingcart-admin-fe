import firebase from 'firebase/app';
import 'firebase/storage';

// Initialize Firebase
var firebaseConfig = {
  apiKey: "AIzaSyAc__JuDLf5_PC4xyuve0NGIhKbge5oTxs",
  authDomain: "x-ecommerce-app.firebaseapp.com",
  databaseURL: "https://x-ecommerce-app.firebaseio.com",
  projectId: "x-ecommerce-app",
  storageBucket: "x-ecommerce-app.appspot.com",
  messagingSenderId: "302569517808",
  appId: "1:302569517808:web:035b29916edb35972e0835",
  measurementId: "G-KX1L0QRPST"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
const storage = firebase.storage();

export {
  storage, firebase as default
}
